#!/usr/bin/env python3

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import NoSuchElementException
import urllib.parse as urlParse
from datetime import date
from datetime import timedelta
import os
import configparser

# config things
dir_path = os.path.dirname(os.path.realpath(__file__))
configParser = configparser.ConfigParser()
configParser.read(dir_path + '/config.ini')
config = configParser['Default']

# last day function


def last_day(today, day_name):
    days_of_week = ['ISO Week day starts from 1',
                    'monday',
                    'tuesday',
                    'wednesday',
                    'thursday',
                    'friday',
                    'saturday',
                    'sunday']
    target_day = days_of_week.index(day_name.lower())
    today_isoweekday = today.isoweekday()

    if today_isoweekday == target_day:
        return today
    elif today_isoweekday < target_day:
        return today - timedelta(days=7 - (target_day - today_isoweekday))
    else:
        return today - timedelta(days=today_isoweekday - target_day)


# variables
today = date.today()
cell_day = config['CellDay']
last_cell_date = (last_day(today, cell_day)).strftime("%d %b %Y")
leader = config['LeaderName']
members = config['Members'].split(',')
present = []
absent = []

# selenium driver things
try:
    s = ChromeService(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=s)
except:
    print("Chrome driver not found")
    pass
    try:
        s = FirefoxService(GeckoDriverManager().install())
        driver = webdriver.Firefox(service=s)
    except:
        print("Firefox driver not found")
        pass

# cell attendance
# go through the list and ask user yes or no, if n ask for reason
print('Type Y if present and reason if not')
for m in members:
    ans = input('Present? ({}) '.format(m))
    if ans and ans[0].lower() == 'y':
        present.append('{}. {}'.format(len(present) + 1, m))
    else:
        if not ans:
            reason = input('Reason? ')
        else:
            reason = ans
        absent.append('{}. {} ({})'.format(len(absent) + 1, m, reason))

# service attendance
service = input('Service Attendance (out of {}) > '.format(len(members)))

# build whatsapp url
url = 'https://api.whatsapp.com/send?text='
url += urlParse.quote(
    '*Date:* {}\n*Leader:* {}\n\n*Cell Present*\n'.format(last_cell_date, leader))
url += urlParse.quote('\n'.join(map(str, present))+'\n\n')
if len(absent) > 0:
    url += urlParse.quote('*Cell Absent*\n'+'\n'.join(map(str, absent))+'\n\n')
url += urlParse.quote('*Livestream*\n{}/{}'.format(service, len(members)))
if int(service) < len(members):
    url += urlParse.quote('\n\n*Deadstream*\n')
    print('service', int(service))
    for x in range(1, (len(members) - int(service) + 1)):
        mia = input('Who was missing? Reason? (name, reason) > ')
        missing = mia.split(',')
        url += urlParse.quote('{}. {} ({})\n'.format(x,
                                                     missing[0], missing[1].strip()))

print(url)

# cell attendance for the form
cell = len(present) + 1


# automate the cell attendance form
try:
    driver.get('https://www.fcbc.org.sg/weeklycellreport')
except:
    driver.get(
        'https://docs.google.com/forms/d/e/1FAIpQLSddXMuJGzKoR_XUQ3ur9OHQ8JU0mXyp8ejAGoA21k54lTtF4Q/viewform')

# start fill up the form
try:
    driver.find_element(By.XPATH,
                        "//div["+config['TeamPastor']+"]/label/div/div/div/div[3]/div").click()
    driver.find_element(By.XPATH,
                        "//div[2]/div/div/div[2]/div/div/div/div/input").click()
    driver.find_element(By.XPATH,
                        "//div[2]/div/div/div[2]/div/div/div/div/input").send_keys(leader)
    driver.find_element(By.XPATH,
                        "//div[4]/div/div/div[2]/div/div/span/div/div/label/div/div/div/div[3]/div").click()
    driver.find_element(By.XPATH,
                        "//div[5]/div/div/div[2]/div/div/div/div/input").send_keys(cell)
    driver.find_element(By.XPATH,
                        "//div[6]/div/div/div[2]/div/div/div/div/input").send_keys("0")
    # check if youth ministry
    if (config.getboolean('IsYouthMinistry')):
        driver.find_element(By.XPATH,
                            "//div[7]/div/div/div[2]/div/div/span/div/div[1]/label/div/div/div/div[3]/div").click()
    else:
        driver.find_element(By.XPATH,
                            "//div[7]/div/div/div[2]/div/div/span/div/div[2]/label/div/div/div/div[3]/div").click()
    driver.find_element(By.XPATH,
                        "//div[9]/div/div/div[2]/div/div/span/div/div/label/div/div/div/div[3]/div").click()
    driver.find_element(By.XPATH,
                        "//div[10]/div/div/div[2]/div/div/div/div/input").send_keys(service)
    driver.find_element(By.XPATH,
                        "//div[11]/div/div/div[2]/div/div/div/div/input").send_keys("0")
    driver.find_element(By.XPATH,
                        "//div[12]/div/div/div[2]/div/div/div/div/input").send_keys("0")
except NoSuchElementException:
    print('No element found')

# fire whatsapp api
driver.execute_script("window.open('{}')".format(url))
