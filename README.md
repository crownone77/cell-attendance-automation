# Cell Attendance Automation

This project is to automate the filling up of the cell attendance form and to send a template whatsapp attendance

## Config Values
**CellDay**: Day you ran cell (e.g. saturday)

**LeaderName**: Cell leader's name

**Members**: List of members name delimited by comma (e.g. Tom,John,Mary)

**IsYouthMinistry**: Whether cell is in Youth Ministry, 13-19 years old. (yes/no)

**TeamPastor**: Number that is in list (e.g. for RLLF is 9, GHMQ is 12)

# TODO
- [ ] Find some online chromedriver. Docker?
- [ ] Think more about UX
